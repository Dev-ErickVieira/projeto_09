$(function(){

	var amtDepoimento = $('.quote').length;
	
	var indiceAtual = 0;

	iniciarDepoimento();
	alternarDepoimento();

	function iniciarDepoimento(){
		$('.quote').hide();
		$('.quote').eq(0).show();
	}

	function alternarDepoimento(){
		$('[next]').click(function(){
			if (indiceAtual < amtDepoimento-1) {
				indiceAtual++;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			}else{
				indiceAtual = 0;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			}
		});

		$('[prev]').click(function(){
			if (indiceAtual > 0) {
				indiceAtual--;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			} else{
				indiceAtual = 0;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			}
		});

	}

});