$(function(){


	//atualizar o navegador sempre que ocorrer um resize.

	var timer;
	var directory = 'http://localhost/Projetos/Projeto_06/';

	
	$(window).resize(function(){

		clearTimeout(timer);

		if (location.href == directory+'home') {

			timer = setTimeout(function(){
				location.href = directory+'home';
			},1000);	

		} else if(location.href == directory+'sobre'){

			timer = setTimeout(function(){
				location.href = directory+'sobre';
			},1000);

		} else if(location.href == directory+'servicos'){

			timer = setTimeout(function(){
				location.href = directory+'servicos';
			},1000);	

		}
	});	
	

	var map;

	//inicializa o Mapa

	function initialize(){

		var mapProp = {

			center:new google.maps.LatLng(-22.933216,-43.238145),
			scrollWheel:false,
			zoom:13,
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("map"),mapProp);
	}

	//Adiciona uma Marca no Mapa
	
	function addMarker(lat,long,icon,content){
		var latLng = {
			lat:lat,
			lng:long
		};

		var marker = new google.maps.Marker({
			position:latLng,
			map:map,
			icon:icon
		});

		var infoWindow = new google.maps.InfoWindow({
			content:content,
			maxWidth:600,
			pixelOffset:new google.maps.Size(0,20)
		});

		infoWindow.open(map,marker);
	}

	var conteudo = '<p style="color:black;font-size:16px;border-bottom:1px solid blue;">Graxa & Cia<br>R.Barão de Mesquita,821 - Andarai</p>';

	initialize();
	addMarker(-22.918643,-43.236554,'',conteudo);
	
})
