<header>
	<div class="container">
		<div class="header-logo">
			
		</div><!--header-logo-->
		<nav class="menu-desktop">
			<ul>
				<li><a href="home">home</a></li>
				<li><a href="sobre">sobre</a></li>
				<li><a href="servicos">servicos</a></li>
				<li><a href="endereco">endereco</a></li>
				<li><a goto="contato" href="">contato</a></li>
			</ul>
		</nav><!--menu-desktop-->
		<div class="clear"></div><!--clear-->
		<nav class="menu-mobile">
			<ul>
				<li><a href="home">Home</a></li>
				<li><a href="sobre">Sobre</a></li>
				<li><a href="galeria">Galeria</a></li>
				<li><a href="endereco">Endereco</a></li>
				<li><a goto="contato" href="">Contato</a></li>
			</ul>
		</nav><!--menu-mobile-->
	</div><!--container-->
</header>

<?php
	if (isset($_GET['url'])) { //se estiver definido.

		$url = $_GET['url'];

		if (file_exists($url.'.html')) {
			include($url.'.html');
		} else{
			include('404.html');
		}

	}else{
		include('home.html');
	}
?>

<footer>
	<div class="container">
		<p>Todos os direitos reservados à Graxa & Cia</p>
	</div><!--container-->	
</footer>
